# 更新说明
> 2019-04-13
 
 升级jfinal版本到3.8,商品管理后台，添加订单详情页面

> 2019-04-11

完善部署文档，增加在idea中集成tomcat运行后台项目文档，增加小程序端运行说明文档

> 2019-04-05

添加系统管理功能：可设置文件存储方式，本地存储或阿里云存储，可设置小程序参数配置
修改后台启动方式，改为mvn命令启动

> 2018-12-23

添加小程序微信支付功能
修改后台工程支持eclipse开发
修改vue后台管理工程配置，允许配置开发环境和生产环境的服务接口地址

> 2018-08-26

完善小程序首页商品列表，新增今日折扣，人气销量，新品首发商品展示页面，新增商品分类商品列表页面

> 2018-07-17

完成小程序下单，登录，获取用户信息，客服，个人中心，优惠券领取，订单列表等功能，完善后台商品列表，订单列表等功能


> 2018-07-12

上传小程序源码，下一步完成下单和完善首页商品浏览
﻿


# QQ交流群
![输入图片说明](https://images.gitee.com/uploads/images/2018/0712/231130_2598d82b_722346.png "jfinal-cms-shop交流群群二维码.png")

群号：699760941

# **此项目仅用于学习交流，如需商用请微信联系：Touch-sea** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0412/130839_db677ec1_95365.png "屏幕截图.png")





# jfinal-cms-shop

#### 项目介绍
基于jfinal+element ui+vue的开源商城管理系统
演示地址：http://gongzhen.site


#### 软件架构
软件架构说明
后台采用java语言，jfinal开源框架 数据库 采用 mysql 5.7

#### 后台项目启动教程

1.导入数据库脚本jfinal_cms/jfinal_cms.sql到mysql

2.导入jfinal-cms项目到idea开发工具，maven项目 

3.更改项目数据库连接,用户名和密码:
   修改 admin,api,model三个模块的配置文件a_little_config.txt   
   
4.在jfinal-cms根目录执行：

   `mvn install package`  
   下载maven依赖jar包
   mvn命令可以git bash执行或者cmd命令行执行

5.启动admin项目：cms-admin
   进入到cms-admin目录：执行mvn命令：

   `mvn tomcat7:run`

 访问：http://localhost:8081/cms-admin/getMenu 可返回菜单数据
 执行`mvn tomcat7:shutdown`，关闭tomcat

6.启动为微信小程序接口项目cms-api
进入到cms-api目录：执行mvn命令：

   `mvn tomcat7:run`

 访问：http://localhost:8080/cms-api/ ，可返回启动成功信息


7.idea开发可把命令添加到启动配置上，一键启动或者debug
![输入图片说明](https://gitee.com/uploads/images/2019/0405/220800_71bcee99_722346.png "2019-04-05_220344.png")

8.在idea集成tomcat，将admin和api模块发布到tomcat上运行
![输入图片说明](https://gitee.com/uploads/images/2019/0411/210743_cf2c4179_95365.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2019/0411/210503_3c93c255_95365.png "屏幕截图.png")

管理后台：http://localhost:8080/cms-admin/getMenu
小程序API：http://localhost:8080/cms-api

9.项目编译打包命令:可生成war包，在tomcat部署

`mvn package`



#### 前端管理界面项目启动说明

1. vue-admin-web为商城管理前端项目
   config/host-dev.json为开发环境后台接口地址
   config/host-prod.json 为发布后的生产环境后台接口地址

2. 进入vue-admin-web目录，安装依赖
```
   npm install  //安装依赖
```

3. 启动项目
```
   npm run dev  //开发环境启动服务，开发时，通过此命令运行
```
   启动成功后，会自动打开页面：http://localhost:1024/#/login ， 需要正常登陆需要启动后台admin项目。
   如果是在idea集成tomcat启动，需要修改一下服务器地址

   ![输入图片说明](https://gitee.com/uploads/images/2019/0411/213517_84416c24_95365.png "屏幕截图.png")

4. 打包发布
```
   npm run build   //生产环境打包，开发完成编译打包部署到tomcat或者nginx
```


#### 小程序项目启动说明
1. 微信开发者工具导入小程序项目jfinal-cms-wx

2. 服务器请求地址，app.js文件

![输入图片说明](https://gitee.com/uploads/images/2019/0411/214126_6c3688a6_95365.png "屏幕截图.png")

3. 运行，注意去除ssl验证


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 项目介绍

 **功能列表** 
1. 商品管理：
商品列表
订单管理
商品类目
商品规格
2. 文章管理：
美妆推荐
今日快报
3. 推广管理：
广告管理
促销
4. 素材管理
5. 用户管理
6. 管理员
7. 栏目管理

 **项目展示** 
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180041_9e6f9e94_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180142_9d532164_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180200_dbf7acc0_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180211_5f083ad3_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180225_d10ef43c_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180245_43ab47ac_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180303_d4c12327_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180325_864111d6_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180336_1e47382b_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180345_27618d71_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180357_d2854e2d_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180421_0f99d77b_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180436_1b204438_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0625/180453_2dd32aba_722346.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0717/182506_0df927a8_722346.png "屏幕截图.png")

 **小程序端** 
![输入图片说明](https://gitee.com/uploads/images/2018/0626/161510_b580d819_722346.jpeg "微信图片_20180625181100.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0626/161613_35250b7d_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0626/161639_adf4dbb6_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0627/173550_aed64b9c_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0627/173416_dd9736c7_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0627/173515_3d0da25d_722346.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0628/221714_ae90b806_722346.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0712/225351_b3bf8231_722346.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0712/225408_ca99d27f_722346.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0717/181902_bba6269e_722346.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0717/181918_6403fd1a_722346.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0826/193729_4226a74d_722346.jpeg "2018-08-26_173356.jpg")  
![输入图片说明](https://images.gitee.com/uploads/images/2018/0826/193746_158e4c2f_722346.jpeg "2018-08-26_173435.jpg")                                                                                                                                                                                                                                                                                                                                          